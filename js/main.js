document.getElementById('myForm').addEventListener('submit', saveBookmark);

function saveBookmark(e){
    let projectName = document.getElementById('projectName').value;
    let startDate = document.getElementById('startDate').value;

    let bookmark = {
        name: projectName,
        sdate: startDate
    }

    if(localStorage.getItem('details') === null){
        let details = [];
        details.push(bookmark);
        localStorage.setItem('details', JSON.stringify(details));
    }
    else{
        let details = JSON.parse(localStorage.getItem('details'));
        details.push(bookmark);
        localStorage.setItem('details', JSON.stringify(details));
    }
    document.getElementById('myForm').reset();
    fetchBookmark();
    e.preventDefault();
}
function deleteUrl(sdate){
    let details = JSON.parse(localStorage.getItem('details'));
    for(let i=0; i<details.length; i++){
        if(details[i].sdate == sdate){
            details.splice(i, 1);
        }
    }
    localStorage.setItem('details', JSON.stringify(details));
    fetchBookmark();
}
    

function fetchBookmark(){
    let details = JSON.parse(localStorage.getItem('details'));
    let projectDetails = document.getElementById('projectDetails');
    projectDetails.innerHTML = '';
    for(let i = 0; i<details.length; i++){
        let name = details[i].name;
        let sdate = details[i].sdate;

        projectDetails.innerHTML += '<div class="well text-center">'+
                                    '<h3>'+name+'</h3>'+
                                    '<h3>'+sdate+'</h3>'+
                                    '<a onclick="deleteUrl(\''+sdate+'\')" class="btn btn-danger" href="#">Delete</a>'
                                    '</div>';
    }
}
